import React, {Component} from 'react';
import axios from 'axios';
import {API_URL,REQUEST_URL} from '../customConfigs/constants';
import PostBlock from '../components/post';
import {Modal} from 'antd';

const confirm = Modal.confirm;


class PostsList extends Component {
    constructor(props){
        super(props);
        this.deletePostHandler = this.deletePostHandler.bind(this);
    }


    deletePostHandler(event) {
    event.preventDefault();
    const Id = event.target.id;
    let deleteHandler =  this.props;
      confirm({
        title: 'Do you want to delete these post ?',
        content: 'When clicked the OK button,  the article will be deleted !',
        okType: 'danger',
        okText: 'Yes',
        cancelText: 'No',
        onOk() {
          return new Promise((resolve, reject) => {
            axios.delete(API_URL + REQUEST_URL + '/' + Id)
                .then((response) => {
                    if(response.data.success) {
                        setTimeout(() => resolve(deleteHandler.postDeleteHandler(Id)), 1500);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })

          })
          .catch(() => console.log('Oops errors!'));

        },
        onCancel() {},
      });
    }


    render() {
        if(this.props.posts) {
            return this.props.posts.map( post => {
                return <div key={post.id}>
                        <PostBlock post={post}
                                  editPostHandler={this.props.editPostHandler}  
                                  deletePostHandler={this.deletePostHandler}  
                            />
                       </div>     
            })
        }

    }
}


export default PostsList;
