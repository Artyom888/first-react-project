import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import { Menu} from 'antd';
import '../App.css';



class TopMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 'page header', 
            imgSrc: 'http://demo.sodhanalibrary.com/images/twitter_blue.png'

        }
        this.handleMouseOver = this.handleMouseOver.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
    }
  
     handleMouseOver() {
        this.setState({
            imgSrc: 'http://demo.sodhanalibrary.com/images/twitter_brown.png'
        });
      }

      handleMouseOut() {
        this.setState({
          imgSrc: 'http://demo.sodhanalibrary.com/images/twitter_blue.png'
        });
      }




    handleClick = (e) => {
        this.setState({
            current: e.key,
        });
    };
    render(){
        return(
            <div>
                <Menu
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"              
                >
                    <Menu.Item >
                        <Link to="/">HOME</Link>
                    </Menu.Item>
                    <Menu.Item >
                        <Link to="/posts">POSTS</Link>
                    </Menu.Item>
                    <Menu.Item >
                        <Link to="/create">CREATE POST</Link>
                    </Menu.Item>
                </Menu>
                <div >
                   <img className="headrerImg" alt="logo"  onMouseOver={this.handleMouseOver} onMouseOut={this.handleMouseOut} src={this.state.imgSrc}/>
                </div>
            </div>
        )

    }

}
export default TopMenu;
